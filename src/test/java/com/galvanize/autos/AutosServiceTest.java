package com.galvanize.autos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AutosServiceTest {

    private AutosService autosService;

    @Mock
    AutosRepository autosRepository;

    @BeforeEach
    void setUp() {
        autosService = new AutosService(autosRepository);
    }

    @Test
    void getAutosReturnsAllAutos() {
        Automobile automobile = new Automobile(1955, "Ford", "Fairlane", "AABBCC", "Sky Blue");
        when(autosRepository.findAll()).thenReturn(Arrays.asList(automobile));
        AutosList autoList = autosService.getAutos();
        assertThat(autoList).isNotNull();
        assertThat(autoList.isEmpty()).isFalse();

    }

    @Test
    void getAutosSearchReturnsList() {
        Automobile automobile = new Automobile(1955, "Ford", "Fairlane", "AABBCC", "Sky Blue");
        automobile.setColor("RED");
        when(autosRepository.findByColorContainsAndMakeContains(anyString(),anyString()))
                .thenReturn(Arrays.asList(automobile));
        AutosList autoList = autosService.getAutos("RED", "Ford");
        assertThat(autoList).isNotNull();
        assertThat(autoList.isEmpty()).isFalse();
    }

    @Test
    void addAutoValidReturnsAuto() {
        Automobile automobile = new Automobile(1955, "Ford", "Fairlane", "AABBCC", "Sky Blue");
        automobile.setColor("RED");
        when(autosRepository.save(any(Automobile.class)))
                .thenReturn(automobile);
        Automobile auto = autosService.addAuto(automobile);
        assertThat(auto).isNotNull();
        assertThat(auto.getMake()).isEqualTo("Ford");

    }

    @Test
    void getAutoWithVINReturnsAuto() {
        Automobile automobile = new Automobile(1955, "Ford", "Fairlane", "AABBCC", "Sky Blue");
        automobile.setColor("RED");
        when(autosRepository.findByVin(anyString()))
                .thenReturn(Optional.of(automobile));
        Automobile auto = autosService.getAuto(automobile.getVin());
        assertThat(auto).isNotNull();
        assertThat(auto.getVin()).isEqualTo(automobile.getVin());
    }

    @Test
    void updateAutoValidReturnsAuto() {
        Automobile automobile = new Automobile(1955, "Ford", "Fairlane", "AABBCC", "Sky Blue");
        automobile.setColor("RED");
        when(autosRepository.findByVin(anyString()))
                .thenReturn(Optional.of(automobile));
        when(autosRepository.save(any(Automobile.class))).thenReturn(automobile);
        Automobile auto = autosService.updateAuto(automobile.getVin(),"PURPLE", "ANYBODY");
        assertThat(auto).isNotNull();
        assertThat(auto.getVin()).isEqualTo(automobile.getVin());
    }

    @Test
    void deleteAutoByVIN() {
        Automobile automobile = new Automobile(1955, "Ford", "Fairlane", "AABBCC", "Sky Blue");
        automobile.setColor("RED");
        when(autosRepository.findByVin(anyString()))
                .thenReturn(Optional.of(automobile));
        autosService.deleteAuto(automobile.getVin());

        verify(autosRepository).delete(any(Automobile.class));

    }

    @Test
    void deleteAutoByVINNoContent(){

        when(autosRepository.findByVin(anyString())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NoContentAutoException.class)
                .isThrownBy(() -> {
                    autosService.deleteAuto("NO-COTENT-VIN");
                });
    }



}
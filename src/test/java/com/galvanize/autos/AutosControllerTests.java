package com.galvanize.autos;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;


import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AutosController.class)
public class AutosControllerTests {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    AutosService autosService;

    ObjectMapper mapper =  new ObjectMapper();

//**********************************************************************************************************************
    //GET: /api/autos
//**********************************************************************************************************************

    //------------------------------------------------------------------------------------------------------------------
    // GET: /api/autos Returns list of all autos
    //------------------------------------------------------------------------------------------------------------------

    @Test
    void getAutosNoArgsExistsReturnsAutosLists() throws Exception{
        //Arrange
        List<Automobile> automobiles = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
        automobiles.add(new Automobile(1967+i, "Ford", "Mustang", "AABB"+i, "red" ));
        }

        when(autosService.getAutos()).thenReturn(new AutosList(automobiles));
        //Act
        mockMvc.perform(get("/api/autos"))
                .andDo(print())
            //Assert
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));

    }
    //------------------------------------------------------------------------------------------------------------------
    // GET: /api/autos Returns 204, no autos found if DB is empty
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void getAutosNoArgsNoneExistReturnsNoContent() throws Exception{
        //Arrange
        when(autosService.getAutos()).thenReturn(new AutosList());
        //Act
        mockMvc.perform(get("/api/autos"))
                .andDo(print())
        //Assert
                .andExpect(status().isNoContent());

    }
    //------------------------------------------------------------------------------------------------------------------
    // GET: /api/autos/color?("Red,Green,Black,etc..) Returns color of auto
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void getAutosSearchColorArgReturnsAutosListColor() throws Exception{
        //arrange
        List<Automobile> automobiles = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            automobiles.add(new Automobile(1967+i, "Ford", "Mustang", "AABB"+i, "red" ));
        }

        when(autosService.getAutos(anyString())).thenReturn(new AutosList(automobiles));
        //act
        mockMvc.perform(get("/api/autos?color=red"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));
        //assert

    }
    //------------------------------------------------------------------------------------------------------------------
    // GET: /api/autos/make?("Ford, Toyota, etc..") Returns autos of same make
    //------------------------------------------------------------------------------------------------------------------

    @Test
    public void getAutosSearchMakeArgReturnsAutosListMake() throws Exception{
        //arrange
        List<Automobile> automobiles = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            automobiles.add(new Automobile(1967+i, "Ford", "Mustang", "AABB"+i, null ));
        }

        when(autosService.getAutos(anyString())).thenReturn(new AutosList(automobiles));
        //act
        mockMvc.perform(get("/api/autos?make=Ford"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));
        //assert

    }

    //------------------------------------------------------------------------------------------------------------------
    // GET: /api/autos/color=Silver&make=Toyota Returns Color and Make
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void getAutosSearchArgsExistsReturnsAutosList() throws Exception{
        //Arrange
        List<Automobile> automobiles = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            automobiles.add(new Automobile(1967+i, "Ford", "Mustang", "AABB"+i, "red" ));
        }
        when(autosService.getAutos(anyString(), anyString())).thenReturn(new AutosList(automobiles));
        //Act
        mockMvc.perform(get("/api/autos?color=red&make=Ford"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.automobiles", hasSize(5)));
    }


//**********************************************************************************************************************
    //POST: /api/autos
//**********************************************************************************************************************

    //------------------------------------------------------------------------------------------------------------------
    // POST: /api/autos Returns created auto
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void addAutoValidReturnsAuto() throws Exception{

    Automobile automobile = new Automobile(1967, "Ford", "Mustang", "AABBCC", "Red");

    when(autosService.addAuto(any(Automobile.class))).thenReturn(automobile);

    mockMvc.perform(post("/api/autos").contentType(MediaType.APPLICATION_JSON)
                                                .content(mapper.writeValueAsString(automobile)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("make").value("Ford"));

    }

    //------------------------------------------------------------------------------------------------------------------
    // POST: /api/autos Returns error message 400, bad request
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void AddAutoBadRequestReturns400() throws Exception{

        Automobile automobile = new Automobile(1967, "Ford", "Mustang", "AABBCC", "Red");

        when(autosService.addAuto(any(Automobile.class))).thenThrow(InvalidAutoException.class);

        mockMvc.perform(post("/api/autos").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(automobile)))
                .andDo(print())
                .andExpect(status().isBadRequest());
   }


//GET: /api/autos/{vin}

    //------------------------------------------------------------------------------------------------------------------
    // GET: /api/autos/{vin} Returns the requested vehicle
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void getAutoWithVINReturnsAuto() throws Exception {

        Automobile automobile = new Automobile(1967, "Ford", "Mustang", "AABBCC", "Red");
        when(autosService.getAuto(anyString())).thenReturn(automobile);

        mockMvc.perform(get("/api/autos/" + automobile.getVin()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("vin").value(automobile.getVin()));
    }

    //------------------------------------------------------------------------------------------------------------------
    // GET: /api/autos/{vin} Returns error message 204, no content
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void getAutoBadRequestReturns204() throws Exception {

        Automobile automobile = new Automobile(1967, "Ford", "Mustang", "AABBCC", "Red");

        when(autosService.getAuto(anyString())).thenThrow(NoContentAutoException.class);

        mockMvc.perform(get("/api/autos/" + automobile.getVin()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

//**********************************************************************************************************************
    //PATCH: /api/autos{vin}
//**********************************************************************************************************************

    //------------------------------------------------------------------------------------------------------------------
    // PATCH: /api/autos{vin} Returns modified vehicle
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void updateAutoWithVINReturnsAuto() throws Exception{

        Automobile automobile = new Automobile(1967, "Ford", "Mustang", "AABBCC", "Red");

        when(autosService.updateAuto(anyString(), anyString(), anyString())).thenReturn(automobile);

        mockMvc.perform(patch("/api/autos/"+automobile.getVin())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"color\":\"RED\", \"owner\":\"Bob\"}"))

            .andExpect(status().isOk())
            .andExpect(jsonPath("color").value("RED"))
            .andExpect(jsonPath("owner").value("Bob"));

    }


    //------------------------------------------------------------------------------------------------------------------
    // PATCH: /api/autos{vin} Returns 204 NoContent
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void updateAutoNoContentReturns204() throws Exception{

        Automobile automobile = new Automobile(1967, "Ford", "Mustang", "AABBCC", "Red");

        when(autosService.updateAuto(anyString(),anyString(),anyString())).thenThrow(NoContentAutoException.class);

        mockMvc.perform(patch("/api/autos/"+automobile.getVin()).contentType(MediaType.APPLICATION_JSON)
                        .content("{\"color\":\"RED\", \"owner\":\"Bob\"}"))
                .andDo(print())
                .andExpect(status().isNoContent());
    }
    //------------------------------------------------------------------------------------------------------------------
    // PATCH: /api/autos{vin} Returns 400 Bad Request
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void updateAutoBadRequestReturns400() throws Exception{

        Automobile automobile = new Automobile(1967, "Ford", "Mustang", "AABBCC", "Red");

        when(autosService.updateAuto(anyString(),anyString(),anyString())).thenThrow(InvalidAutoException.class);

        mockMvc.perform(patch("/api/autos/"+automobile.getVin()).contentType(MediaType.APPLICATION_JSON)
                        .content("{\"color\":\"RED\", \"owner\":\"Bob\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

//**********************************************************************************************************************
    //DELETE: /api/autos{vin}
// **********************************************************************************************************************

    //------------------------------------------------------------------------------------------------------------------
    // DELETE: /api/autos{vin} Returns 202, delete request accepted
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void deleteAutoWithVinExistsReturns202() throws Exception{


        mockMvc.perform(delete("/api/autos/AABBCC"))
                .andExpect(status().isAccepted());
        verify(autosService).deleteAuto(anyString());

    }

    //------------------------------------------------------------------------------------------------------------------
    // DELETE: /api/autos{vin} Returns 204, vehicle not found.
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void deleteAutoWithVINNotExistReturns204() throws Exception{

        doThrow(new NoContentAutoException()).when(autosService).deleteAuto(anyString());
        mockMvc.perform(delete("/api/autos/AABBCC"))
                .andExpect(status().isNoContent());
        verify(autosService).deleteAuto(anyString());
    }


}
